
AXthe is an opinionated JAMstack based tech stack for high productivity web app development.

#### Please star our repo
- http://github.com/axthe/axthe
 
#### Support and Forum
- http://github.com/axthe/axthe/discussions
 
# Setup
If you already have node.js setup, greater than version 14.17.6 (npm greater than 7.20.0), with Caddy http server running in the cloud and a Cloud based IDE, you are good to go, you can skip to guide.
 
- For latest: http://github.com/axthe/axthe and click Code/Download ZIP), or use wget from shell to download the file.
In root of the extracted files run caddy, in  folder rename file _env to ```.env```. Running ```dev.sh``` should start your appi app; last in caddy run ```rdev.sh```.


 
### Domain and server
- You need A DNS name/domain, I recommend Porkbun.com or http://easydns.com (and I recommend against GoDaddy), so get(or transfer) a domain that you can control.
- A $5-$10 / month ubuntu based OS server in the cloud. You can pick anything similar to http://vultr.com, http://primcast.com, http://hetzner.com, http://soyoustart.com, Digital Ocean, Linode, etc. Likely that anything Ubuntu in the cloud will do the job.
 
 
## Setup recipe
After you create an Ubuntu instance in the cloud:
You should edit in the cloud, sign up for a cloud based IDE, I recommend http://codeanywhere.com/editor. For  mostly use a Cloud IDE, and for components or mobile|cordova development you could use local VS Code with the SSH plugin (  http://code.visualstudio.com/docs/remote/ssh F1 then 'SSH connect window to a remote host')
 
- Use the Cloud IDE (CodeAnywhere) after you open the IDE, connect to the Ubuntu instance in the cloud via ssh. You should see the editor and be able to ssh. Like this:
 
<img src="ide.png" />
 
And this is the screen to setup the connection, select ssh:
 
<img src="ide_setup.png" width="400"/>
 
- Ubuntu config:
 
run the setup script: http://github.com/AXthe/AXthe/blob/main/setup.sh, you can cut and paste, line by line. I cut and paste a few line in at a time (into the Cloud IDE). As a part of setup you should make sure you are running Python v3.X and not the older version.
 
or you can try, but better to cut/paste every few lines: #wget -O - https://raw.githubusercontent.com/axthe/axthe/main/setup.sh | bash  


- Then create Caddyfile similar to this:
http://github.com/axthe/axthe/blob/main/Caddyfile
 
and
```
caddy start
```
or caddy stop as needed. Everytime you change the Caddyfile you need to stop and start it.

Configure a sub domain in your dns to go there! :-)
 
You may need to glance the Caddy docs on their web site, but: you must know how to connect a DNS to your server. That is this step, don't go to next step till you are comfortable connecting N number of apps installed on your linux server to DNS, so that you can go to it from your browser via HTTPS.

- There are several folders, you should connect each to Caddy+DNS. Likely you want to rename  and  folders to your app names, so that you can write more than one app, or have more than one version. The is the end of the setup!
 
You should now know how to setup up Caddy+DNS to a subdomain.
 
## How to upgrade
 
From time to time we update our AXthe stack, environment and scripts. Here is how to upgrade to a newer version:
1. Setup a new Ubuntu instance in the cloud as per above.
2. Edit Caddyfile so you have new subdomains, for example version2.mydomain.com, and get it to just do hello world type stuff.
3. Just copy and paste your files over the new AXthe stack.
5. Now you can move your DNS around as needed, for example you have your old box with older code and your new box with newer code. Maybe use legacy.mydomain.com to point to the old app, and alpha.mydomain.com to point to the new app.
 
 
### As needed shell commands that you can cut/paste:
 
```
ufw status
 
killall node
 
ps aux | grep node
 
nvm alias default 14.17.1
```
 
